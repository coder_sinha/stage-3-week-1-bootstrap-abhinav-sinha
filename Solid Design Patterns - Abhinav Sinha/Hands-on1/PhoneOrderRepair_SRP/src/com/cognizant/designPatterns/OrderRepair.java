package com.cognizant.designPatterns;

import java.util.Scanner;

public class OrderRepair {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Welcome to our site. Would you like to order or repair?");
		Scanner sc = new Scanner(System.in);
		String processOpt = sc.nextLine().toLowerCase().trim();
		
		String product = null;

		switch (processOpt) {
		
		case "repair":
			System.out.println("Is it the phone or the accessory that you want to be repaired?");
			String type = sc.nextLine().toLowerCase().trim();
			
			if(type.equals("phone")) {
				System.out.println("Please provide the phone model name");
				PhoneRepair phoneRepair = new PhoneRepair();
				product = sc.nextLine().trim();
				phoneRepair.ProcessPhoneRepair(product);
			}
			else {
				System.out.println("Please provide the accessory detail, like headphone, tempered glass");
				AccessoryRepair accessoryRepair = new AccessoryRepair();
				product = sc.nextLine().trim();
				accessoryRepair.ProcessAccessoryRepair(product);
			}
			break;
			
		case "order":
			System.out.println("Please provide the phone model name");
			product = sc.nextLine().trim();
			PhoneOrder phoneOrder = new PhoneOrder();
			phoneOrder.ProcessOrder(product);
			break;
			
		default:
			break;
		}
		
		sc.close();
	}

}
