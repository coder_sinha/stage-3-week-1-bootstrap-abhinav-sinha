package com.cognizant.designPatterns;

import java.util.Scanner;

public class OrderRepair {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Welcome to our site. Would you like to order or repair?");
		Scanner scanner = new Scanner(System.in);
		String processOpt = scanner.nextLine().toLowerCase().trim();
		
		String product = null;
		PhoneOrderRepair phoneOrderRepair = new PhoneOrderRepair();

		switch (processOpt) {
		
		case "repair":
			System.out.println("Is it the phone or the accessory that you want to be repaired?");
			String type = scanner.nextLine().toLowerCase().trim();
			if(type.equals("phone")) {
				System.out.println("Please provide the phone model name");
				product = scanner.nextLine().trim();
				phoneOrderRepair.ProcessPhoneRepair(product);
			}
			else {
				System.out.println("Please provide the accessory detail, like headphone, tempered glass");
				product = scanner.nextLine().trim();
				phoneOrderRepair.ProcessAccessoryRepair(product);
			}
			break;
			
		case "order":
			System.out.println("Please provide the phone model name");
			product = scanner.nextLine().trim();
			phoneOrderRepair.ProcessOrder(product);
			break;
			
		default:
			break;
		}
		
		scanner.close();
	}

}
