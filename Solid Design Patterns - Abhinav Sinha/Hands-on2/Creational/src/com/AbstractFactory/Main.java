package com.AbstractFactory;

import java.util.Scanner;

public final class Main 
{
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		String brand = scanner.next().toLowerCase().trim();				
		
		Client client = new Client();
		client.doSomeClientJob(brand);
		
		scanner.close();
	}
}