package com.Singleton;

//SingleTon class
public class DBConn {

	// static private member – ‘instance’ to hold its own instance
	private static DBConn connection = null;

	// Private constructor - to make only a single instance
	private DBConn() {

	}

	// Static function.
	public static DBConn getConnnection() {

		// If there is no instance available, create new one (i.e. lazy initialization).
		if (connection == null) {
			connection = new DBConn();
		}
		return connection;
	}

}
