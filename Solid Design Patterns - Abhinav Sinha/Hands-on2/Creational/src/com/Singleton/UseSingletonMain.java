package com.Singleton;

public class UseSingletonMain {
	public static void main(String[] args) {

		DBConn connection1 = DBConn.getConnnection();
		System.out.println(connection1);

		DBConn connection2 = DBConn.getConnnection();
		System.out.println(connection2);

		System.out.println(connection1 == connection2);

	}

}
