
public class MusdAdapterImp implements Meuro {
	private Musd usd;
	
	public double getCurr() {
		return convertUSDtoEURO(usd.getCurr());
	}
	
	public MusdAdapterImp(Musd usd) {
		super();
		this.usd = usd;
	}
	
	private double convertUSDtoEURO(double cur) {
        return cur * 0.924967;
    }

}
