import java.util.ArrayList;
import java.util.List;

public class Meal {
   private List<Item> list = new ArrayList<Item>();	

   public void addItem(Item item){
      list.add(item);
   }


   public void showItems(){
   
      for (Item item : list) {
         System.out.print("Item : " + item.name());
         System.out.print(", Packing : " + item.packing().pack());
         System.out.println(", Price : " + item.price());
      }		
   }	
   
   public float getCost(){
	   float cost = 0.0f;
	   
	   for (Item item : list) {
		   cost += item.price();
	   }		
	   return cost;
   }
}