public class BuilderPatternDemo {
   public static void main(String[] args) {
   
      MealBuilder mealBuilder = new MealBuilder();

      Meal nonVeg = mealBuilder.prepareNonVegMeal();
      System.out.println("Non-Veg Meal");
      
      nonVeg.showItems();
      System.out.println("Total Cost: " + nonVeg.getCost());
      
      Meal veg = mealBuilder.prepareVegMeal();
      System.out.println("Veg Meal");
      
      veg.showItems();
      System.out.println("Total Cost: " + veg.getCost());

   }
}