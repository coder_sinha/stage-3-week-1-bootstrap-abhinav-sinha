package com.abstractFactory;

public class CarFactory {
	private CarFactory()  
    { 
          
    } 
    public static Car buildCar(CarType carType,Location location) 
    { 
        Car car = null; 
     
        switch(location) 
        { 
            case INDIA: 
                car = INDIACarFactory.buildCar(carType); 
                break; 
                      
            case USA: 
            	car = USACarFactory.buildCar(carType); 
            	break; 
            	
            default: 
                car = DefaultCarFactory.buildCar(carType); 
  
        } 
          
        return car; 
  
    }
}
