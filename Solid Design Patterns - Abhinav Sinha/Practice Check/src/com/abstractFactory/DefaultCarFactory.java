package com.abstractFactory;

public class DefaultCarFactory {
	public static Car buildCar(CarType carType) 
    { 
        Car car = null; 
        switch (carType) 
        { 
	        case LUXURY: 
	        	car = new LuxuryCar(Location.DEFAULT); 
	        	break; 
	        	
            case MICRO: 
                car = new MicroCar(Location.DEFAULT); 
                break; 
              
            case MINI: 
                car = new MiniCar(Location.DEFAULT); 
                break; 
                  
                  
                default: 
                break; 
              
        } 
        return car; 
    }
}
