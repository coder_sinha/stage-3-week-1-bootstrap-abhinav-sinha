package com.ObserverPatt;

public class Main {

	   public static void main(String[] args) {
	    JohnObserver so=new JohnObserver("Chandan");
	    JohnObserver jo=new JohnObserver("Piyush");
	    
	    NotificationService NS=new NotificationService();
	    
	  
	    NS.registerObserver(so);
	    NS.registerObserver(jo);
	    NS.notifyObservers();
	    System.out.println();
	    
	    NS.removeObserver(jo);
	    NS.notifyObservers();
	   }
	}