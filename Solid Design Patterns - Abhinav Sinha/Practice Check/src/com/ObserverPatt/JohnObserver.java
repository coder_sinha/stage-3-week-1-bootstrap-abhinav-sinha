package com.ObserverPatt;

public class JohnObserver implements INotificationObserver {

	String name;

	public JohnObserver(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void update() {

		System.out.println("Hello " + name + ", Notification has been received");
	}
}