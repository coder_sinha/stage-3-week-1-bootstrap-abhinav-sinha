package chainOfResponsibiltiy;

public class ProjectManager implements ILeaveRequestHandler { 
    private ILeaveRequestHandler nextInChain; 
  
    public void setHandler(ILeaveRequestHandler nextInChain) 
    { 
        this.nextInChain = nextInChain; 
    } 
  
    public void Handlerequest(LeaveRequest request) 
    { 
        if (request.getNoOfLeaveDays() >= 3 && request.getNoOfLeaveDays() < 5)  
        { 
            System.out.println("Request  for " + request.getNoOfLeaveDays() + " days of " + request.getEmployeeName() + " sent to Project Manager"); 
        } 
        else
        { 
            nextInChain.Handlerequest(request); 
        } 
    }

}
