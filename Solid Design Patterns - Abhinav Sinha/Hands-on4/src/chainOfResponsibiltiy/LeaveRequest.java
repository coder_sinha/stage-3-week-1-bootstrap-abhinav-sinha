package chainOfResponsibiltiy;

public class LeaveRequest {
	
	String employeeName;
	int noOfLeaveDays;
	
	public LeaveRequest(String employeeName, int noOfLeaveDays) {
		super();
		this.employeeName = employeeName;
		this.noOfLeaveDays = noOfLeaveDays;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public int getNoOfLeaveDays() {
		return noOfLeaveDays;
	}

	public void setNoOfLeaveDays(int noOfLeaveDays) {
		this.noOfLeaveDays = noOfLeaveDays;
	}
}
