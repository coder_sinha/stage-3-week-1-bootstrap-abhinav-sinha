package chainOfResponsibiltiy;

public class Main {
	LeaveRequest lr;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ILeaveRequestHandler nextInChain1 = new Supervisor();
		ILeaveRequestHandler nextInChain2 = new ProjectManager();
		ILeaveRequestHandler nextInChain3 = new HR();
		
		nextInChain1.setHandler(nextInChain2);
		nextInChain2.setHandler(nextInChain3);
		
		nextInChain1.Handlerequest(new LeaveRequest("Rajesh",1)); 
        nextInChain1.Handlerequest(new LeaveRequest("Vikas",4)); 
        nextInChain1.Handlerequest(new LeaveRequest("Abhi",8));		
	}

}
