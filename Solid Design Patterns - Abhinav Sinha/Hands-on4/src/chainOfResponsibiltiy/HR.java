package chainOfResponsibiltiy;

public class HR implements ILeaveRequestHandler { 
    private ILeaveRequestHandler nextInChain; 
  
    public void setHandler(ILeaveRequestHandler nextInChain) 
    { 
        this.nextInChain = nextInChain; 
    } 
  
    public void Handlerequest(LeaveRequest request) 
    { 
        if (request.getNoOfLeaveDays() >= 5) 
        { 
            System.out.println("Request  for " + request.getNoOfLeaveDays() + " days of " + request.getEmployeeName() + " sent to HR section"); 
        } 
        else
        { 
            nextInChain.Handlerequest(request); 
        } 
    }

}
