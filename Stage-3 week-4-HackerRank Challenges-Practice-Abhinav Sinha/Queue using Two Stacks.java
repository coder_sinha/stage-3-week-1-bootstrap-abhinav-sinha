import java.io.*;
import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Stack<Integer> s1 = new Stack<>();
        Stack<Integer> s2 = new Stack<>();
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t>0){
            int type = sc.nextInt();
            if(type == 1){
                int input = sc.nextInt();
                s1.push(input);
            } else {
                if(s2.empty()){
                    while(!s1.empty()){
                        s2.push(s1.pop());
                    }
                }
                if(type == 2)
                    s2.pop();
                else 
                    System.out.println(s2.peek());
            }
            t--;
        }
    }
}
