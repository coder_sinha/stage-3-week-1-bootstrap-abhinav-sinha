import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the isBalanced function below.
    static String isBalanced(String a) {

        Stack<Character> x = new Stack<>();
        char s[] = a.toCharArray();

        for(int i=0;i<s.length;i++){
            if(x.empty()){
                x.push(s[i]);
            } else {
                if(x.peek() == '['){
                    if(s[i] == '{' || s[i] == '(' || s[i] == '[')
                        x.push(s[i]);
                    else if(s[i] == ']')
                        x.pop();
                    else {
                        return "NO";
                    }
                } else if(x.peek() == '{'){
                    if(s[i] == '{' || s[i] == '(' || s[i] == '[')
                        x.push(s[i]);
                    else if(s[i] == '}')
                        x.pop();
                    else {
                        return "NO";
                    }
                } else if(x.peek() == '('){
                    if(s[i] == '{' || s[i] == '(' || s[i] == '[')
                        x.push(s[i]);
                    else if(s[i] == ')')
                        x.pop();
                    else {
                        return "NO";
                    }
                }
            }
        }
        if(x.empty())
            return "YES";
        else
            return "NO";

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String s = scanner.nextLine();

            String result = isBalanced(s);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}

