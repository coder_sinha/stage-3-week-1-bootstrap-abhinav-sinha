import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        
        String temp[] = s.split(":");
        String ans="";
        int h = Integer.parseInt(temp[0]);

        if(temp[2].contains("A")){
            if(h == 12){
                ans += "00:"+temp[1]+":"+temp[2].replace("AM","");
            } else {
                if(h < 10)
                    ans += "0"+h+":"+temp[1]+":"+temp[2].replace("AM","");
                else
                    ans += h+":"+temp[1]+":"+temp[2].replace("AM","");
            }
        } else {
            if(h == 12){
                ans += "12:"+temp[1]+":"+temp[2].replace("PM","");
            } else {
                h = h+12;
                ans += h+":"+temp[1]+":"+temp[2].replace("PM","");
            }
        }

        return ans;

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}

