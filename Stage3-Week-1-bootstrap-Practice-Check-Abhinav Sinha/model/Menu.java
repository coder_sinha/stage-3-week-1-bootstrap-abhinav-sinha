package com.cognizant.truyum.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Entity
@Table(name = "menu_item")
public class Menu {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "me_id")
	private int id;

	@Column(name = "me_name")
	private String name;

	@Column(name = "me_price")
	private BigDecimal price;

	@Column(name = "me_active")
	private boolean active;

	@DateTimeFormat(iso = ISO.DATE)
	@Column(name = "me_date_of_launch")
	private Date launchDate;

	@Column(name = "me_category")
	private String category;

	@Column(name = "me_free_delivery")
	private boolean freeDelivery;

	@Column(name = "me_image_url")
	private String imageUrl;

	public Menu() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Menu(int id, String name, BigDecimal price, boolean active, Date launchDate, String category,
			boolean freeDelivery, String imageUrl) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.active = active;
		this.launchDate = launchDate;
		this.category = category;
		this.freeDelivery = freeDelivery;
		this.imageUrl = imageUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getLaunchDate() {
		return launchDate;
	}

	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean isFreeDelivery() {
		return freeDelivery;
	}

	public void setFreeDelivery(boolean freeDelivery) {
		this.freeDelivery = freeDelivery;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	@Override
	public String toString() {
		return "Menu [id=" + id + ", name=" + name + ", price=" + price + ", active=" + active + ", launchDate="
				+ launchDate + ", category=" + category + ", freeDelivery=" + freeDelivery + ", imageUrl=" + imageUrl
				+ "]";
	}

}
